import random
import pygame
import sys
import time
import os 
import re
import json 

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
done = False

def isAValidHexColor(string):
    pattern = "^[#][a-fA-F0-9]{6}$|^[a-fA-F0-9]{6}$"
    return re.match(pattern, string)

def getPyColors(): 
    try: 
        with open("settings.json", "r") as f: 
            colors = []
            data = f.read()
            obj = json.loads(data)
            
            colors.append(obj['WALL'])
            colors.append(obj['VISITED'])
            colors.append(obj['SCREEN_BACKGROUND'])
            colors.append(obj['CURRENT_CELL'])
            colors.append(obj['STARTING_CELL'])
            colors.append(obj['EXIT_CELL'])
            colors.append(obj['PATH_COLOR'])
            colors.append(obj['VISITED_BUT_NOT_ON_PATH'])

        for c in colors:
            if not(isAValidHexColor(c)):
                print(f"Error : {c} is not a valid hex color.", file=sys.stderr)
                raise ValueError(c + " is not a valid hex color.")

    except:
        print("Error : Using default colors", file=sys.stderr)
        colors = [
            "#FFFFFF", 
            "#6031E0",
            "#333333",
            "#00FF00",
            "#00CDFF",
            "#CC0000",
            "#0086FF",
            "#554F64"
        ]

    pyColors = []
    for c in colors: 
        pyColors.append(hexToRGB(c))

    return pyColors

def myRand(a, b):
    return random.randint(a, b)

def isBetween(x, a, b):
    return (min(a, b) <= x) and (x <= max(a, b))

def hexToRGB(hexColor):
    h = hexColor.replace("#", "")
    r = h[0:2]
    g = h[2:4]
    b = h[4:6]

    red = int(r, 16)
    green = int(g, 16)
    blue = int(b, 16)

    return pygame.Color(red, green, blue)

def getRandomElement(myList):
    randomIndex = myRand(0, len(myList)-1)
    return myList[randomIndex]

def getCellWidth(rows, cols):
    infoObject = pygame.display.Info()
    screenWidth = infoObject.current_w
    screenHeight = infoObject.current_h

    w = 2 

    while (w * cols < screenHeight) and (w * rows < screenWidth):
        w += 1

    return w-1  

pyColors = getPyColors()
WALL = pyColors[0]
VISITED = pyColors[1]
SCREEN_BACKGROUND = pyColors[2]
CURRENT_CELL = pyColors[3]
STARTING_CELL = pyColors[4]
EXIT_CELL = pyColors[5]

class Cell:
    def __init__(self, i, j):
        self.i = i
        self.j = j
        self.topWall = True
        self.rightWall = True
        self.bottomWall = True
        self.leftWall = True
        self.visited = False
        self.highlight = False

    def removeWall(self, cell):
        if (self.j == cell.j - 1):
            self.bottomWall = False
            cell.topWall = False

        elif (self.j == cell.j + 1):
            self.topWall = False
            cell.bottomWall = False

        elif (self.i == cell.i - 1):
            self.rightWall = False
            cell.leftWall = False

        elif (self.i == cell.i + 1):
            self.leftWall = False
            cell.rightWall = False

        return

class Maze:
    def __init__(self, rows, cols):
        self.cols = cols
        self.rows = rows
        self.grid = []
        self.cellWidth = cellWidth

        for i in range(rows):
            for j in range(cols):
                self.grid.append(Cell(i, j))

    def validCoordinates(self, i, j):
        return (isBetween(i, 0, self.rows - 1)) and (isBetween(j, 0, self.cols - 1))

    def getIndex(self, i, j):
        if self.validCoordinates(i, j):
            index = j + i * self.cols

        else:
            index = -1

        return index

    def show(self):
        w = self.cellWidth
        pygame.event.get()

        for i in range(self.rows):
            for j in range(self.cols):
                event = pygame.event.poll()

                if (event.type == pygame.QUIT):
                    pygame.quit()
                    sys.exit(0)

                index = self.getIndex(i, j)
                currentCell = self.grid[index]
                x = currentCell.i * w
                y = currentCell.j * w

                if (currentCell.visited):
                    rect = pygame.Rect(x, y, w, w)
                    pygame.draw.rect(screen, VISITED, rect)

                if (currentCell.topWall):
                    pygame.draw.line(screen, WALL, (x, y), (x + w, y))

                if (currentCell.rightWall):
                    pygame.draw.line(screen, WALL, (x + w, y), (x + w, y + w))

                if (currentCell.bottomWall):
                    pygame.draw.line(screen, WALL, (x + w, y + w), (x, y + w))

                if (currentCell.leftWall):
                    pygame.draw.line(screen, WALL, (x, y + w), (x, y))

                if (currentCell.highlight):
                    rect = pygame.Rect(x, y, w, w)
                    pygame.draw.rect(screen, CURRENT_CELL, rect)

        radius = int(w//4)

        coordinates = (2*radius, 2*radius)
        pygame.draw.circle(screen, STARTING_CELL, coordinates, radius)

        index = self.getIndex(self.rows-1, self.cols-1)
        currentCell = self.grid[index]
        x = currentCell.i * w
        y = currentCell.j * w

        coordinates = (int(x + 2*radius), int(y + 2*radius))

        pygame.draw.circle(
            screen, EXIT_CELL, coordinates, radius)
        pygame.display.flip()
        return

    def getCell(self, i, j):
        index = self.getIndex(i, j)

        if index >= 0:
            cell = self.grid[index]

        else:
            cell = None

        return cell

    def getNeighbors(self, i, j):
        neighbors = []

        top = self.getCell(i-1, j)
        right = self.getCell(i, j+1)
        bottom = self.getCell(i+1, j)
        left = self.getCell(i, j-1)

        for n in [top, right, bottom, left]:
            if (n != None):
                neighbors.append(n)

        return neighbors

    def getUnvisitedNeighbors(self, i, j):
        unvisitedNeighbors = []
        neighbors = self.getNeighbors(i, j)

        for n in neighbors:
            if (n != None) and not(n.visited):
                unvisitedNeighbors.append(n)

        if (len(unvisitedNeighbors) > 0):
            randomIndex = myRand(0, len(unvisitedNeighbors) - 1)
            u = unvisitedNeighbors[randomIndex]
        else:
            u = None

        return u

    def getWalls(self, i, j):
        walls = []
        wallStr = "{}-{}-".format(i, j)
        index = self.getIndex(i, j)
        cell = self.grid[index]

        if (cell.topWall):
            walls.append(wallStr + "T")

        if (cell.rightWall):
            walls.append(wallStr + "R")

        if (cell.bottomWall):
            walls.append(wallStr + "B")

        if (cell.leftWall):
            walls.append(wallStr + "L")

        return walls

    def checkNeighbor(self, wallStr):
        split = wallStr.split("-")
        i = int(split[0], base=10)
        j = int(split[1], base=10)
        wall = split[2]

        if (wall == "T"):
            neighbor = self.getCell(i, j-1)

        elif (wall == "R"):
            neighbor = self.getCell(i+1, j)

        elif (wall == "B"):
            neighbor = self.getCell(i, j+1)

        elif (wall == "L"):
            neighbor = self.getCell(i-1, j)

        else:
            print("Unknown wall : {} --> {}".format(wallStr, wall))
            sys.exit(1)

        return (neighbor != None) and (not(neighbor.visited))

    def generer(self):
        current = self.grid[0]
        current.visited = True
        wallsList = []
        done = False
        change = False

        walls = self.getWalls(current.i, current.j)
        for w in walls:
            wallsList.append(w)

        while (not(done)):
            pygame.event.pump()

            if (change):
                self.show()

            if (len(wallsList) > 0):
                change = True
                randomIndex = myRand(0, len(wallsList)-1)
                randomWall = wallsList[randomIndex]

                split = randomWall.split("-")
                i = int(split[0])
                j = int(split[1])
                wall = split[2]

                current = self.getCell(i, j)

                if (wall == "T"):
                    neighbor = self.getCell(i-1, j)

                elif (wall == "R"):
                    neighbor = self.getCell(i, j+1)

                elif (wall == "B"):
                    neighbor = self.getCell(i+1, j)

                elif (wall == "L"):
                    neighbor = self.getCell(i, j-1)

                else:
                    print("Unknown wall : " + randomWall +
                          " --> " + wall, file=sys.stderr)
                    sys.exit(1)

                if (neighbor != None) and (not(neighbor.visited)):
                    neighbor.visited = True
                    current.removeWall(neighbor)

                    walls = self.getWalls(neighbor.i, neighbor.j)
                    for w in walls:
                        wallsList.append(w)

                else:
                    wallsList.pop(randomIndex)
                    change = False
            else:
                done = True
        return

if len(sys.argv) > 2:
    rows = int(sys.argv[1], base=10)
    cols = int(sys.argv[2], base=10)

else:
    rows = int(input("Rows : "), base=10)
    cols = int(input("Cols : "), base=10)

cellWidth = getCellWidth(rows, cols)
screenHeight = cellWidth * cols 
screenWidth = cellWidth * rows 
screen = pygame.display.set_mode((screenWidth, screenHeight))
screen.fill(SCREEN_BACKGROUND)
m = Maze(rows, cols)
m.show()
m.generer()

while True:
    event = pygame.event.poll()
    if (event.type == pygame.QUIT):
        pygame.quit()
        sys.exit(0)

    else:
        m.show()
