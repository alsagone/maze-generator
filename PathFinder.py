import random
import pygame
import sys
import time
import os
import json 
import re

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
done = False

def isAValidHexColor(string):
    pattern = "^[#][a-fA-F0-9]{6}$|^[a-fA-F0-9]{6}$"
    return re.match(pattern, string)

def getPyColors(): 
    try: 
        with open("settings.json", "r") as f: 
            colors = []
            data = f.read()
            obj = json.loads(data)
            
            colors.append(obj['WALL'])
            colors.append(obj['VISITED'])
            colors.append(obj['SCREEN_BACKGROUND'])
            colors.append(obj['CURRENT_CELL'])
            colors.append(obj['STARTING_CELL'])
            colors.append(obj['EXIT_CELL'])
            colors.append(obj['PATH_COLOR'])
            colors.append(obj['VISITED_BUT_NOT_ON_PATH'])

        for c in colors:
            if not(isAValidHexColor(c)):
                print(f"Error : {c} is not a valid hex color.", file=sys.stderr)
                raise ValueError(c + " is not a valid hex color.")

    except:
        print("Error : Using default colors", file=sys.stderr)
        colors = [
            "#FFFFFF", 
            "#6031E0",
            "#333333",
            "#00FF00",
            "#00CDFF",
            "#CC0000",
            "#0086FF",
            "#554F64"
        ]

    pyColors = []
    for c in colors: 
        pyColors.append(hexToRGB(c))

    return pyColors

def myRand(a, b):
    return random.randint(a, b)


def isBetween(x, a, b):
    return (min(a, b) <= x) and (x <= max(a, b))


def hexToRGB(hexColor):
    h = hexColor.replace("#", "")
    r = h[0:2]
    g = h[2:4]
    b = h[4:6]

    red = int(r, 16)
    green = int(g, 16)
    blue = int(b, 16)

    return pygame.Color(red, green, blue)

def getRandomElement(myList):
    if len(myList) > 0:
        randomIndex = myRand(0, len(myList) - 1)
        randomElement = myList[randomIndex]
    else:
        randomElement = None

    return randomElement


def getCellWidth(rows, cols):
    infoObject = pygame.display.Info()
    screenWidth = infoObject.current_w
    screenHeight = infoObject.current_h

    w = 2

    while (w * cols < screenHeight) and (w * rows < screenWidth):
        w += 1

    return w-1


class Cell:
    def __init__(self, i, j):
        self.i = i
        self.j = j
        self.topWall = True
        self.rightWall = True
        self.bottomWall = True
        self.leftWall = True
        self.visited = False
        self.highlight = False
        self.onPath = False
        self.visitedButNotOnPath = False
        self.manhattanDistance = 0

    def __repr__(self):
        return "({}, {})".format(self.i, self.j)

    def removeWall(self, cell):
        if self.j == cell.j - 1:
            self.bottomWall = False
            cell.topWall = False

        elif self.j == cell.j + 1:
            self.topWall = False
            cell.bottomWall = False

        elif self.i == cell.i - 1:
            self.rightWall = False
            cell.leftWall = False

        elif self.i == cell.i + 1:
            self.leftWall = False
            cell.rightWall = False

        return

    def isReachable(self, goal):
        if self.j == goal.j - 1:
            reachable = (not(self.bottomWall) and not(goal.topWall))

        elif self.j == goal.j + 1:
            reachable = (not(self.topWall) and not(goal.bottomWall))

        elif self.i == goal.i - 1:
            reachable = (not(self.rightWall) and not(goal.leftWall))

        elif self.i == goal.i + 1:
            reachable = (not(self.leftWall) and not(goal.rightWall))

        else:
            reachable = False

        return reachable

    def getManhattanDistance(self, goal):
        return abs(goal.i - self.i) + abs(goal.j - self.j)


def getClosestCell(cellList):
    closestCell = None
    minDistance = float("+inf")

    for cell in cellList:
        if (cell.manhattanDistance < minDistance):
            closestCell = cell 
            minDistance = cell.manhattanDistance

    return cell 


pyColors = getPyColors()
WALL = pyColors[0]
VISITED = pyColors[1]
SCREEN_BACKGROUND = pyColors[2]
CURRENT_CELL = pyColors[3]
STARTING_CELL = pyColors[4]
EXIT_CELL = pyColors[5]
PATH_COLOR = pyColors[6]
VISITED_BUT_NOT_ON_PATH = pyColors[7]

class Maze:
    def __init__(self, rows, cols):
        self.cols = cols
        self.rows = rows
        self.grid = []
        self.cellWidth = cellWidth

        for i in range(rows):
            for j in range(cols):
                self.grid.append(Cell(i, j))

        endIndex = self.getIndex(rows-1, cols-1)
        endCell = self.grid[endIndex]

        for i in range(0, endIndex+1, 1):
            self.grid[i].manhattanDistance = self.grid[i].getManhattanDistance(endCell)


    def validCoordinates(self, i, j):
        return (isBetween(i, 0, self.rows - 1)) and (isBetween(j, 0, self.cols - 1))

    def getIndex(self, i, j):
        if self.validCoordinates(i, j):
            index = j + i * self.cols

        else:
            index = -1

        return index

    def show(self):
        w = self.cellWidth
        pygame.event.get()

        for i in range(self.rows):
            for j in range(self.cols):
                event = pygame.event.poll()

                if (event.type == pygame.QUIT):
                    pygame.quit()
                    sys.exit(0)
                index = self.getIndex(i, j)
                currentCell = self.grid[index]
                x = currentCell.i * w
                y = currentCell.j * w
                rect = pygame.Rect(x, y, w, w)

                if currentCell.visited:
                    pygame.draw.rect(screen, VISITED, rect)

                if currentCell.highlight:
                    pygame.draw.rect(screen, CURRENT_CELL, rect)

                if currentCell.onPath:
                    pygame.draw.rect(screen, PATH_COLOR, rect)

                elif currentCell.visitedButNotOnPath:
                    pygame.draw.rect(screen, VISITED_BUT_NOT_ON_PATH, rect)

                if currentCell.topWall:
                    pygame.draw.line(screen, WALL, (x, y), (x + w, y))

                if currentCell.rightWall:
                    pygame.draw.line(screen, WALL, (x + w, y), (x + w, y + w))

                if currentCell.bottomWall:
                    pygame.draw.line(screen, WALL, (x + w, y + w), (x, y + w))

                if currentCell.leftWall:
                    pygame.draw.line(screen, WALL, (x, y + w), (x, y))

        radius = int(w//4)

        coordinates = (2*radius, 2*radius)
        pygame.draw.circle(screen, STARTING_CELL, coordinates, radius)

        index = self.getIndex(self.rows-1, self.cols-1)
        currentCell = self.grid[index]
        x = currentCell.i * w
        y = currentCell.j * w

        coordinates = (int(x + 2*radius), int(y + 2*radius))

        pygame.draw.circle(
            screen, EXIT_CELL, coordinates, radius)
        pygame.display.flip()
        return

    def getCell(self, i, j):
        index = self.getIndex(i, j)

        if (index >= 0):
            cell = self.grid[index]

        else:
            cell = None

        return cell

    def getNeighbors(self, i, j):
        neighbors = []

        top = self.getCell(i-1, j)
        right = self.getCell(i, j+1)
        bottom = self.getCell(i+1, j)
        left = self.getCell(i, j-1)

        for n in [top, right, bottom, left]:
            if n:
                neighbors.append(n)

        return neighbors

    def getUnvisitedNeighbors(self, i, j):
        unvisitedNeighbors = []
        neighbors = self.getNeighbors(i, j)

        for n in neighbors:
            if (n != None) and not(n.visited):
                unvisitedNeighbors.append(n)

        return unvisitedNeighbors

    def getVisitedNeighbors(self, i, j):
        visitedNeighbors = []
        neighbors = self.getNeighbors(i, j)

        for n in neighbors:
            if ((n != None) and n.visited):
                visitedNeighbors.append(n)

        return visitedNeighbors

    def getReachableNeighbors(self, cell):
        neighbors = self.getNeighbors(cell.i, cell.j)
        reachableNeighbors = []

        for n in neighbors:
            if cell.isReachable(n) and not(n.visitedButNotOnPath) and not(n.onPath):
                reachableNeighbors.append(n)

        return reachableNeighbors

    def findMatch(self):
        found = False
        for i in range(self.rows):
            for j in range(self.cols):
                cell = self.getCell(i, j)
                if not(cell.visited):
                    visitedNeighbors = self.getVisitedNeighbors(i, j)
                    found = len(visitedNeighbors) > 0

                if (found):
                    break

            if (found):
                break

        if (found):
            neighbor = visitedNeighbors[0]
            cell.removeWall(neighbor)
            return neighbor

        else:
            return None

    def generer(self, show):
        current = self.grid[0]
        current.visited = True
        done = False

        while (not(done)):
            pygame.event.pump()

            if (show):
                current.highlight = True
                self.show()
                current.highlight = False

            unvisitedNeighbors = self.getUnvisitedNeighbors(
                current.i, current.j)

            if (len(unvisitedNeighbors) > 0):
                nextCell = getRandomElement(unvisitedNeighbors)
                nextCell.visited = True
                current.removeWall(nextCell)
                current = nextCell

            else:
                nextCell = self.findMatch()

                if (nextCell != None):
                    current = nextCell

                else:
                    done = True

        return

    def findPathManhattan(self):
        current = self.grid[0]
        current.onPath = True
        stack = []

        endIndex = self.getIndex(rows-1, cols-1)
        end = self.grid[endIndex]
        endFound = False

        while (not(endFound)):
            pygame.event.pump()
            current.onPath = True
            current.highlight = True
            """time.sleep(0.1)"""
            self.show()
            current.highlight = False

            reachableNeighbors = self.getReachableNeighbors(current)

            if (len(reachableNeighbors) > 0):
                if (end in reachableNeighbors):
                    print("End found")
                    end.onPath = True
                    endFound = True
                else:
                    nextCell = getClosestCell(reachableNeighbors)
                    stack.append(current)
                    current = nextCell

            elif (len(stack) > 0):
                current.onPath = False
                current.visitedButNotOnPath = True
                current = stack.pop()

            else:
                print("Stack is empty, unable to find the end", file=sys.stderr)
                sys.exit(1)

        return


if len(sys.argv) > 2:
    rows = int(sys.argv[1], base=10)
    cols = int(sys.argv[2], base=10)

else:
    rows = int(input("Rows : "), base=10)
    cols = int(input("Cols : "), base=10)

showGen = ("-s" in sys.argv)

cellWidth = getCellWidth(rows, cols)
m = Maze(rows, cols)
screenHeight = cellWidth * cols
screenWidth = cellWidth * rows
screen = pygame.display.set_mode((screenWidth, screenHeight))
screen.fill(SCREEN_BACKGROUND)
m.generer(showGen)

m.show()
m.findPathManhattan()

while True:
    event = pygame.event.poll()
    if (event.type == pygame.QUIT):
        pygame.quit()
        sys.exit(0)

    else:
        m.show()
