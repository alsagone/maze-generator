# Maze Generator and Path Finder

## Description

Different Python algorithms to generate a maze:

-   Recursive Backtracker
-   Hunt and Kill
-   Randomized Prim's algorithm
-   Bonus : a fullscreen version for the Hunt and Kill algorithm which can be used as a screensaver

You can edit the file `settings.json` to set the colors as you wish.
You can refer to `defaultSettings.json` if you want to revert your changes.

### Requirements

[Pygame library](https://www.pygame.org/wiki/GettingStarted)

#### Usage

    py filename.py [height] [width] [-s]

The option _-s_ shows the maze generation.
